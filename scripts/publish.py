"""
bifulushi
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import argparse
import json
import re
from pathlib import Path
from sys import stderr

VERSION_RE = r"-([a-z\d\.]+)\.xpi$"
SITE = "https://namingthingsishard.gitlab.com/firefox/bifulushi"


def exit_with_message(msg: str, code=0):
    print(msg, file=stderr, flush=True)
    exit(code)


def main(version_dir: Path, site: str, output_file: str):
    if not version_dir.is_dir():
        exit_with_message("No versions directory")

    # Build list of updates
    updates = []
    for version_file in list(version_dir.glob("*.xpi")):
        if not version_file.is_file():
            print("ignoring %s" % version_file)
            continue
        if not (match := re.search(VERSION_RE, version_file.name.lower())):
            continue

        updates.append({
            "version": match.group(1),
            "update_link": f"{site}/{version_file}"
        })

    updates = sorted(updates, key=lambda update: update["version"])
    if not updates:
        exit_with_message("No versions found")

    print("Found versions:\n" + '\n'.join([update["version"] for update in updates]))

    with open(output_file, "w") as update_json:
        # https://extensionworkshop.com/documentation/manage/updating-your-extension/
        json.dump({
            "addons": {
                "bifulushi@namingthingsishard": {
                    "updates": updates
                }
            }
        }, update_json, indent=2)


def dir_path_type(val: str) -> Path:
    if not (path := Path(val)).is_dir():
        raise argparse.ArgumentTypeError("Must be a directory!")

    return path


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--site", default=SITE)
    parser.add_argument("--output", default="update.json")
    parser.add_argument("directory", type=dir_path_type)

    args = parser.parse_args()
    main(args.directory, args.site, args.output)
